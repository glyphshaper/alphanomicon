
function ParalaxObject( trackedObj, xOffset, yOffset ){
	
	this.trackedObj = trackedObj;
	this.xOffset = xOffset;
	this.yOffset = yOffset;
	
}

function ParalaxObjectProcessor(){
	
	this.objs = new Array();
	
	this.registerObject = function( obj ){
		
		this.objs.push( obj );
		
	}
	
	this.doParalaxShift = function(){
		
		var objIndex = this.objs.length;
		var thisObj;
		
		while( --objIndex > -1 ){
			
			thisObj = this.objs[ objIndex ];
			
			// apply the transform to the current object
			// remember you have access to the properties of the object, like so:
			// thisObj.trackedObj
			// thisObj.xOffset
			// thisObj.yOffset
			
		}
		
	}
	
}

var pObjProcessor = new ParalaxObjectProcessor();

var plaxObj1 = new ParalaxObject( document.getElementById( 'div1' ), -20, 0 );
pObjProcessor.registerObject( plaxObj1 );

// set up a timer here...


// timer calls a function that just calls pObjProcessor.doParalaxShift()

