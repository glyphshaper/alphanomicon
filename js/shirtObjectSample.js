function Shirt( sColor ){

	this.color = sColor;
	
}

var redShirt = new Shirt( 'red' );
var blueShirt = new Shirt( 'blue' );

alert( 'The color of redShirt is ' + redShirt.color + '\nThe color of blueShirt is ' + blueShirt.color );