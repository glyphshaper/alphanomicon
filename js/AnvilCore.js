AnvilCore = (function(){
	
	/**********************************
	*	Create a window.console stub if it is not present
	*/
	if( typeof window.console == undefined ){
		window.console = {
			log: function(){},
			debug: function(){},
			dir: function(){},
			trace: function(){}
		};
	};
	
	/**********************************
	*	A generic app level data storage helper designed to hold 'containers' of information
	*	used by various mechanisms in the AnvilCore suite of tools.
	*/
	var Data = function(){
		
		var _dataStore = {};
		
		this.store = function( k, v, containerID ){
			
			// if no container ID is provided, use 'default'
			containerID = containerID || 'default';
			
			// create the data store container if it does not exist
			if( _dataStore[containerID] == undefined ) _dataStore[containerID] = {};
			
			_dataStore[containerID][k] = v;
			
		};
		
		this.getValue = function( k, containerID ){
			
			containerID = containerID || 'default';
			return _dataStore[containerID][k];
		};
		
	};
	
	/**********************************
	*	A form tools object designed to handle various tasks related to form processing and validation
	*/
	var Forms = function(){
		
	};
	
	/**********************************
	*	An observable object that events are dispatched from to be responded to by registered listeners.
	*/
	var Relay = function(){
		
		var _eventRegistry = {};
		var _listenerID = -1;
		
		this.addEventListener = function( evt, handler ){
			
			// create the event registry entry if it does not exist yet
			if( !_eventRegistry[evt] ) _eventRegistry[evt] = [];
			
			// register the submitted handler to this event
			_eventRegistry[evt].push({
				id: (++_listenerID).toString(),
				handler: handler
			});
			
			return _eventRegistry[evt].id;
		};
		
		this.removeEventListener = function( evtID ){
			
			var rtrn = false;
			
			for( var evt in _eventRegistry ){
				for( var i = 0, len = _eventRegistry[evt].length; i < len; i++){
					if( _eventRegistry[evt][i].id === evtID ){
						_eventRegistry[evt].splice( i, 1 );
						rtrn = evtID;
					}
				}
			};
			
			return rtrn;
			
		};
		
		this.dispatch = function( evt, args ){
			
			var evtSuccess = false;
			
			if( _eventRegistry[evt] ){
				
				evtSuccess = true;
				
				setTimeout( function(){
					var listeners = _eventRegistry[evt],
						len = listeners ? listeners.length : 0 ;
					
					while( len-- ){
						listeners[len].handler( evt, args );
					}
				}, 0);
			}
			
			return evtSuccess;
		};
	};
	
	return {
		Data: new Data(),
		Forms: new Forms(),
		Relay: new Relay()
	};
	
})();

/* 	REFERENCES
*	General js design pattern info - http://addyosmani.com/resources/essentialjsdesignpatterns/book/
*/